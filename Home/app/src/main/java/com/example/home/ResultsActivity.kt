package com.example.home

import android.content.Intent
import android.content.res.AssetManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_results.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.URL

class ResultsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        var data : MutableList<ReceptData> = mutableListOf()
        val mIntent = intent
        val obrok = mIntent.getStringExtra("obrok")
        val recept = mIntent.getStringExtra("recept")
        var fileName : String = ""
        if(obrok.equals("dorucak")){
            fileName = "dorucak_info.csv"
        }else if(obrok.equals("rucak")){
            fileName = "rucak_info.csv"
        }else{
            fileName = "vecera_info.csv"
        }
        val am: AssetManager = applicationContext.assets
        val inputStream = am.open(fileName)

        val reader = BufferedReader(InputStreamReader(inputStream))
        var line : String? = ""
        while (reader.readLine().also {  line = it} != null) {
            var array = line?.split(",")
            for(i in array!!){
                data.add(ReceptData(array[0],array[1],array[2],array[3]))
            }
        }
        var recipeData : ReceptData? = null
        for(d in data){
            if(d.ime.equals(recept)){
                recipeData = d
            }
        }
        //Log.d("Link", recipeData?.slika)
        /*
        var url = URL(recipeData?.slika)
        var bmp: Bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
        iv_rezultat.setImageBitmap(bmp)
        */
        Picasso.get().load(recipeData?.slika).into(iv_rezultat)
        tv_ime_recept.text = recipeData?.ime
        tv_kalorije.text = "Kcal po porciji: " + recipeData?.kalorije
        tv_recept.text = "URL recepta: " + recipeData?.recept

        initListeners()
    }

    fun initListeners() {
        btn_povratak.setOnClickListener{
            startActivity(Intent(this, MainActivity::class.java))
        }
    }
}
