package com.example.home

data class ReceptData (
    val ime: String,
    val recept: String,
    val slika: String,
    val kalorije: String
)