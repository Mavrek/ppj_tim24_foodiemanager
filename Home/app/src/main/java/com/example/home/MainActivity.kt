package com.example.home

import android.content.Intent
import android.content.res.AssetManager
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity() {

    var data = ArrayList<Array<String?>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initListeners()
    }

    fun startLearning(fileName : String, testData: Array<String?>) : String? {
        val am: AssetManager = applicationContext.assets
        val inputStream = am.open(fileName)

        val learnReader = BufferedReader(InputStreamReader(inputStream))

        val zk =
            learnReader.readLine().split(",".toRegex()).toTypedArray()
        val broj_zk = zk.size
        val skup_znacajki = arrayOfNulls<String>(broj_zk - 1)
        val int_skup_znacajki = arrayOfNulls<Int>(broj_zk - 1)
        var oznaka_klase: String? = null
        for (i in 0 until broj_zk) {
            if (i == broj_zk - 1) {
                oznaka_klase = zk[i]
            } else {
                skup_znacajki[i] = zk[i]
                int_skup_znacajki[i] = i
            }
        }
        var line: String?
        while (learnReader.readLine().also { line = it } != null) {
            val rowData = arrayOfNulls<String>(broj_zk)
            val value =
                line?.split(",".toRegex(), broj_zk.coerceAtLeast(0))?.toTypedArray()
            for (i in 0 until broj_zk) {
                rowData[i] = value?.get(i)
            }
            data.add(rowData)
        }

        learnReader.close()

        val index_klase: Int = data[0].size- 1
        val vrijednosti_klase: TreeSet<String> = TreeSet()
        for (s in data) {
            s[index_klase]?.let { vrijednosti_klase.add(it) }
        }

        val poredak_klasa: MutableMap<String, Int> = HashMap()
        for ((index, s) in vrijednosti_klase.withIndex()) {
            poredak_klasa[s] = index
        }

        val tree = DecisionTree()
        val current_depth = 0
        val max_depth = 2
        tree.root = null
        tree.root = fit(data, data, int_skup_znacajki, oznaka_klase, skup_znacajki, max_depth, current_depth);
        tree.skupSvihZnacajki = skup_znacajki;

        var result = predict(tree, testData, poredak_klasa);
        Log.d("res", result)
        return result

    }

    private fun predict(tree: DecisionTree, testData: Array<String?>, poredakKlasa: MutableMap<String, Int>) : String? {
        var result = predictTest(tree.root, testData, poredakKlasa)
        Log.d("res2", result)
        return result
    }

    private fun predictTest(currentNode: Node?, testData: Array<String?>, poredakKlasa: MutableMap<String, Int>) : String? {
        val index_klase: Int = testData.size
        if (currentNode is ClassNode) {
            val node = currentNode as ClassNode
            Log.d("predikcija:", node.oznaka_klase)
            return node.oznaka_klase
        } else {
            var check = false
            val node = currentNode as DecisionNode
            val value: String? = testData.get(node.znacajka)
            for (i in 0 until node.vrijednostiZnacajki.size) {
                if (node.vrijednostiZnacajki[i] == value) {
                    check = true
                    return predictTest(node.nodes[i]!!, testData, poredakKlasa)
                }
            }
            if (!check) {
                val broj_ponavljanja_vrijednosti =
                    getPonavljanjeVrijednostiAtributa(node.dataSubset, index_klase)
                var argmax = 0
                var nameargmax = ""
                for ((key, value1) in broj_ponavljanja_vrijednosti) {
                    Log.d("kljuc", key)
                    if (value1 > argmax) {
                        argmax = value1
                        if (key != null) {
                            nameargmax = key
                        }
                    } else if (value1 == argmax) {
                        if (key!!.compareTo(nameargmax!!) < 0) {
                            argmax = value1
                            nameargmax = key
                        }
                    }
                }
                Log.d("predikcija", nameargmax)
                return nameargmax
            }
        }
        return ""
    }

    private fun fit(
        data: java.util.ArrayList<Array<String?>>,
        dataSubset: ArrayList<Array<String?>>,
        intSkupZnacajki: Array<Int?>,
        oznakaKlase: String?,
        skupZnacajki: Array<String?>,
        maxDepth: Int,
        currentDepth: Int
    ): Node? {
        if (dataSubset != null) {
            if (dataSubset.isEmpty()) {
                val temp_index_klase: Int = data[0].size - 1
                val temp_broj_ponavljanja_vrijednosti_klase: Map<String?, Int> =
                    getPonavljanjeVrijednostiAtributa(data, temp_index_klase)
                var tempargmax = 0
                var tempnameargmax = ""
                for ((key, value) in temp_broj_ponavljanja_vrijednosti_klase) {
                    if (value > tempargmax) {
                        tempargmax = value
                        if (key != null) {
                            tempnameargmax = key
                        }
                    } else if (value == tempargmax) {
                        if (key!! < tempnameargmax) {
                            tempargmax = value
                            if (key != null) {
                                tempnameargmax = key
                            }
                        }
                    }
                }
                val classNode = ClassNode()
                classNode.oznaka_klase = tempnameargmax
                return classNode
            }
        }

        val index_klase: Int = dataSubset.get(0).size - 1
        val broj_ponavljanja_vrijednosti_klase: Map<String?, Int> =
            getPonavljanjeVrijednostiAtributa(dataSubset, index_klase)
        var argmax = 0
        var nameargmax = ""
        for ((key, value) in broj_ponavljanja_vrijednosti_klase) {
            if (value > argmax) {
                argmax = value
                if (key != null) {
                    nameargmax = key
                }
            } else if (value == argmax) {
                if (key!! < nameargmax) {
                    argmax = value
                    if (key != null) {
                        nameargmax = key
                    }
                }
            }
        }

        if (maxDepth === currentDepth) {
            val classNode = ClassNode()
            classNode.oznaka_klase = nameargmax
            return classNode
        }


        val dataCheck: ArrayList<Array<String?>> = ArrayList()
        if (dataSubset != null) {
            for (d in dataSubset) {
                if (d[index_klase] == nameargmax) {
                    dataCheck.add(d)
                }
            }
        }

        if (intSkupZnacajki.size === 0 || dataCheck == dataSubset) {
            val classNode = ClassNode()
            classNode.oznaka_klase = nameargmax
            return classNode
        }
        var maxID = -1.0
        var maxIDOfZnacajka = 0
        for (i in intSkupZnacajki) {
            val currID: Double = getID(dataSubset, i)
            if (currID > maxID) {
                maxID = currID
                if (i != null) {
                    maxIDOfZnacajka = i
                }
            } else if (currID == maxID) {
                if (i?.let { skupZnacajki[maxIDOfZnacajka]?.let { it1 ->
                        skupZnacajki[it]?.compareTo(
                            it1
                        )
                    } }!! < 0) {
                    maxIDOfZnacajka = i
                }
            }
        }
        val decisionNode = DecisionNode()
        decisionNode.znacajka = maxIDOfZnacajka
        decisionNode.dataSubset = dataSubset
        decisionNode.depth = currentDepth

        val novi_skup_preostalih_znacajki =
            arrayOfNulls<Int>(intSkupZnacajki.size - 1)
        var index = 0
        for (i in intSkupZnacajki) {
            if (i != maxIDOfZnacajka) {
                novi_skup_preostalih_znacajki[index] = i
                index++
            }
        }

        val newDataSubsets: MutableMap<String?, ArrayList<Array<String?>>> =
            HashMap()
        for (d in dataSubset) {
            val vrijednost = d[maxIDOfZnacajka]
            var partData = newDataSubsets[vrijednost]
            if (partData == null) {
                partData = ArrayList()
                partData.add(d)
                newDataSubsets[vrijednost] = partData
            } else {
                newDataSubsets[vrijednost]!!.add(d)
            }
        }

        decisionNode.nodes = arrayOfNulls(newDataSubsets.size)
        decisionNode.vrijednostiZnacajki = arrayOfNulls(newDataSubsets.size)
        index = 0
        for ((key, value) in newDataSubsets) {
            decisionNode.vrijednostiZnacajki[index] = key
            decisionNode.nodes[index] = fit(
                dataSubset,
                value,
                novi_skup_preostalih_znacajki,
                oznakaKlase,
                skupZnacajki,
                maxDepth,
                currentDepth + 1
            )!!
            index++
        }

        return decisionNode
    }

    private fun getID(dataSubset: ArrayList<Array<String?>>, znacajka: Int?): Double {
        var informacijska_dobit: Double = getEntropy(dataSubset)
        if (informacijska_dobit != 0.0) {
            val vrijednosti_znacajke: ArrayList<String> = ArrayList()
            for (d in dataSubset) {
                if (!vrijednosti_znacajke.contains(d[znacajka!!])) d[znacajka]?.let {
                    vrijednosti_znacajke.add(
                        it
                    )
                }
            }
            for (vrijednost in vrijednosti_znacajke) {
                val dataSubsetZnacajka =
                    ArrayList<Array<String?>>()
                var tempEntropija = 0.0
                var ocekivana_vrijednost = 0.0
                for (d in dataSubset) {
                    if (d[znacajka!!] == vrijednost) {
                        dataSubsetZnacajka.add(d)
                    }
                }
                tempEntropija = getEntropy(dataSubsetZnacajka)
                ocekivana_vrijednost =
                    dataSubsetZnacajka.size.toDouble() / dataSubset.size * tempEntropija
                informacijska_dobit -= ocekivana_vrijednost
            }
            return informacijska_dobit
        }
        return 0.0

    }

    private fun getEntropy(dataSubset: ArrayList<Array<String?>>): Double {
        var entropija = 0.0
        if (dataSubset.size > 0) {
            val index_klase: Int = dataSubset[0].size - 1
            val broj_ponavljanja_vrijednosti_klase =
                getPonavljanjeVrijednostiAtributa(dataSubset, index_klase)
            for (vrijednost in broj_ponavljanja_vrijednosti_klase.keys) {
                val vjerojatnost: Double =
                    broj_ponavljanja_vrijednosti_klase[vrijednost]!!.toDouble() / dataSubset.size
                entropija -= vjerojatnost * Math.log(vjerojatnost) / Math.log(
                    2.0
                )
            }
        }
        return entropija
    }

    private fun getPonavljanjeVrijednostiAtributa(
        dataSubset: ArrayList<Array<String?>>,
        atribut: Int
    ): Map<String?, Int> {
        val broj_ponavljanja_vrijednosti: MutableMap<String?, Int> =
            HashMap()
        for (d in dataSubset) {
            if (broj_ponavljanja_vrijednosti.containsKey(d[atribut]))
                broj_ponavljanja_vrijednosti[d[atribut]] = broj_ponavljanja_vrijednosti[d[atribut]]!! + 1
            else
                broj_ponavljanja_vrijednosti[d[atribut]] = 1
        }
        return broj_ponavljanja_vrijednosti
    }

    fun initListeners() {
        btn_dorucak.setOnClickListener {
            var testData = btnClick()
            var recept = startLearning("dorucak.csv", testData)
            val mIntent = Intent(this, ResultsActivity::class.java)
            mIntent.putExtra("recept",recept)
            mIntent.putExtra("obrok","dorucak")
            startActivity(mIntent)
        }

        btn_rucak.setOnClickListener {
            var testData = btnClick()
            var recept = startLearning("rucak.csv", testData)
            val mIntent = Intent(this, ResultsActivity::class.java)
            mIntent.putExtra("recept",recept)
            mIntent.putExtra("obrok","rucak")
            startActivity(mIntent)
        }

        btn_vecera.setOnClickListener {
            var testData = btnClick()
            var recept = startLearning("vecera.csv", testData)
            val mIntent = Intent(this, ResultsActivity::class.java)
            mIntent.putExtra("recept",recept)
            mIntent.putExtra("obrok","vecera")
            startActivity(mIntent)
        }
    }
    fun btnClick() : Array<String?> {

        var slatko: Float
        var slano: Float
        var gorko: Float
        var kiselo: Float
        var ljuto: Float
        var voce: Float
        var povrce: Float
        var meso: Float
        var zitarice: Float
        var morskiPlodovi: Float
        var mlijecniProizvod: Float
        var tijesto: Float

        if (et_slatko.text.isEmpty()) { slatko = 0.5F }
        else { slatko = et_slatko.text.toString().toFloat() / 10F }
        if (et_slano.text.isEmpty()) { slano = 0.5F }
        else { slano = et_slano.text.toString().toFloat() / 10F }
        if (et_gorko.text.isEmpty()) { gorko = 0.5F }
        else { gorko = et_gorko.text.toString().toFloat() / 10F }
        if (et_kiselo.text.isEmpty()) { kiselo = 0.5F }
        else { kiselo = et_kiselo.text.toString().toFloat() / 10F }
        if (et_ljuto.text.isEmpty()) { ljuto = 0.5F }
        else { ljuto = et_ljuto.text.toString().toFloat() / 10F }
        if (et_voce.text.isEmpty()) { voce = 0.5F }
        else { voce = et_voce.text.toString().toFloat() / 10F }
        if (et_povrce.text.isEmpty()) { povrce = 0.5F }
        else { povrce = et_povrce.text.toString().toFloat() / 10F }
        if (et_meso.text.isEmpty()) { meso = 0.5F }
        else { meso = et_meso.text.toString().toFloat() / 10F }
        if (et_zitarice.text.isEmpty()) { zitarice = 0.5F }
        else { zitarice = et_zitarice.text.toString().toFloat() / 10F }
        if (et_morski_plodovi.text.isEmpty()) { morskiPlodovi = 0.5F }
        else { morskiPlodovi = et_morski_plodovi.text.toString().toFloat() / 10F }
        if (et_mlijecni_proizvod.text.isEmpty()) { mlijecniProizvod = 0.5F }
        else { mlijecniProizvod = et_mlijecni_proizvod.text.toString().toFloat() / 10F}
        if (et_tijesto.text.isEmpty()) { tijesto = 0.5F }
        else { tijesto = et_tijesto.text.toString().toFloat() / 10F}

        if (slatko < 0 || slatko > 1) {
            et_slatko.error = "Broj mora biti između 0 i 10"
            et_slatko.requestFocus()
        }
        if (slano < 0 || slano > 1) {
            et_slano.error = "Broj mora biti između 0 i 10"
            et_slano.requestFocus()
        }
        if (gorko < 0 || gorko > 1) {
            et_gorko.error = "Broj mora biti između 0 i 10"
            et_gorko.requestFocus()
        }
        if (kiselo < 0 || kiselo > 1) {
            et_kiselo.error = "Broj mora biti između 0 i 10"
            et_kiselo.requestFocus()
        }
        if (ljuto < 0 || ljuto > 1) {
            et_ljuto.error = "Broj mora biti između 0 i 10"
            et_ljuto.requestFocus()
        }
        if (voce < 0 || voce > 1) {
            et_voce.error = "Broj mora biti između 0 i 10"
            et_voce.requestFocus()
        }
        if (povrce < 0 || povrce > 1) {
            et_povrce.error = "Broj mora biti između 0 i 10"
            et_povrce.requestFocus()
        }
        if (meso < 0 || meso > 1) {
            et_meso.error = "Broj mora biti između 0 i 10"
            et_meso.requestFocus()
        }
        if (zitarice < 0 || zitarice > 1) {
            et_zitarice.error = "Broj mora biti između 0 i 10"
            et_zitarice.requestFocus()
        }
        if (morskiPlodovi < 0 || morskiPlodovi > 1) {
            et_morski_plodovi.error = "Broj mora biti između 0 i 10"
            et_morski_plodovi.requestFocus()
        }
        if (mlijecniProizvod < 0 || mlijecniProizvod > 1) {
            et_mlijecni_proizvod.error = "Broj mora biti između 0 i 10"
            et_mlijecni_proizvod.requestFocus()
        }
        if (tijesto < 0 || tijesto > 1) {
            et_tijesto.error = "Broj mora biti između 0 i 10"
            et_tijesto.requestFocus()
        }

        var toTest = arrayOfNulls<String>(12)
        toTest[0] = slatko.toString()
        toTest[1] = slano.toString()
        toTest[2] = gorko.toString()
        toTest[3] = ljuto.toString()
        toTest[4] = kiselo.toString()
        toTest[5] = voce.toString()
        toTest[6] = povrce.toString()
        toTest[7] = meso.toString()
        toTest[8] = zitarice.toString()
        toTest[9] = morskiPlodovi.toString()
        toTest[10] = mlijecniProizvod.toString()
        toTest[11] = tijesto.toString()
        for(stavka in toTest)
            print(stavka)
        return toTest
    }
}
