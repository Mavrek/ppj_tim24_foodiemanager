package com.example.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_stats.*

class StatsActivity : AppCompatActivity() {
    var male : Boolean? = null
    var selectedItem : String? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stats)
        initListeners()
    }

    fun initListeners(){
        val activityList = listOf("Sedentary (office job)", "Light Exercise (1-2 days/week)","Moderate Exercise (3-5 days/week)", "Heavy Exercise (6-7 days/week)", "Very Heavy Exercise (2x per day)")
        val activitySpinner = findViewById<Spinner>(R.id.activity_spinner)
        val adapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, activityList)
        activitySpinner.adapter = adapter
        activitySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
               selectedItem = activityList[position]
            }

        }

        calc_btn.setOnClickListener {
            val age = age_text.text.toString().trim()
            val weight = weight_text.text.toString().trim()
            val height = height_text.text.toString().trim()
            if(male == null) {
                Toast.makeText(this, "Select gender first", Toast.LENGTH_SHORT).show()
            }else if(age.isEmpty()){
                age_text.error = "Enter age"
                age_text.requestFocus()
            }else if(weight.isEmpty()){
                weight_text.error = "Enter weight"
                weight_text.requestFocus()
            }else if(height.isEmpty()){
                height_text.error = "Enter height"
                height_text.requestFocus()
            }else{
                val cal = getCalories(male!!,age.toInt(),  weight.toDouble(), height.toInt(), selectedItem!!)
                var intent : Intent = Intent(this,CaloriesActivity::class.java)
                intent.putExtra("CALORIES", cal)
                startActivity(intent)
            }
        }
    }

    fun onRadioButtonClicked(view: View) {
        if (view is RadioButton) {
            val checked = view.isChecked

            when (view.getId()) {
                R.id.male_button ->
                    if (checked) {
                        male = true
                    }
                R.id.female_button ->
                    if (checked) {
                        male = false
                    }
            }
        }
    }

    fun getCalories(male : Boolean, age : Int, weight : Double, height : Int, activity : String) : Int {
        var caloriesBase : Int = 0
        if(male){
            caloriesBase = (66+(13.7*weight)+(5*height)-(6.8*age)).toInt()
        }else{
            caloriesBase = (655+(9.6*weight)+(1.8*height)-(4.7*age)).toInt()
        }
        when(activity) {
            "Sedentary (office job)" -> return (1.2*caloriesBase).toInt()
            "Light Exercise (1-2 days/week)" -> return (1.375*caloriesBase).toInt()
            "Moderate Exercise (3-5 days/week)" -> return (1.55*caloriesBase).toInt()
            "Heavy Exercise (6-7 days/week)" -> return (1.725*caloriesBase).toInt()
            "Very Heavy Exercise (2x per day)" -> return (1.9*caloriesBase).toInt()
            else -> return caloriesBase
        }
    }
}
